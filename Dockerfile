FROM node:10.15.1

RUN mkdir -p /srv/app/application
COPY . /srv/app/application
WORKDIR /srv/app/application


RUN npm i -g yarn
RUN yarn

CMD ["npm", "start"]
