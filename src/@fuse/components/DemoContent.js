import React from 'react';

const DemoContent = () => {
    return (
        <div>

            <img
                src="assets/images/demo-content/morain-lake.jpg"
                alt="beach"
                style={{
                    maxWidth: '640px',
                    width   : '100%',
                    marginLeft:'30%'
                }}
            />

            <h1 className="py-16">Banco Bilbao Vizcaya Argentaria (BBVA)</h1>
            <p>
            La historia de BBVA comienza en 1857 en Bilbao, ciudad situada en el norte de España, cuando la Junta de 
            Comercio promueve la creación de Banco de Bilbao como banco de emisión y descuento. Se trató de una iniciativa pionera,
            impulsada por un contexto de crecimiento económico de la región. Hasta la última década del siglo XIX, la entidad actuó
            casi en solitario en la plaza.
            </p>

            <blockquote>
                <p>
                   En la segunda mitad de siglo XIX, Banco de Bilbao financia importantes proyectos de infraestructuras y de desarrollo
                   siderúrgico en su zona de influencia. En 1878 pierde la facultad de emitir billetes propios y se reorganiza como banco de 
                   préstamos y descuento
                </p>
            </blockquote>

            <p>
                En la década de 1980, Banco de Bilbao basa su estrategia en alcanzar una mayor dimensión que le permita acceder a los negocios 
                financieros que surgen de los avances tecnológicos, la desregulación, la securitización y la interrelación de los mercados nacionales
                e internacionales. Banco de Vizcaya contribuye al reflotamiento de bancos afectados por la crisis económica y desarrolla una política
                de fuerte crecimiento por adquisiciones, que le lleva a formar un gran grupo bancario. La operación más importante es la compra de
                Banca Catalana en 1984. Por su parte, las entidades oficiales de crédito van ampliando sus negocios con operaciones de mercado.
                En 1982, BEX pierde la exclusividad del crédito a la exportación, orienta su negocio hacia la banca universal y forma un grupo financiero.
                En este proceso adquiere Banco de Alicante (1983).


            </p>

            <p>
                Tras varios años dedicado al crecimiento exterior, BBVA aprovecha la reestructuración del sistema financiero en España para crecer especialmente
                en Cataluña, una de las regiones donde tenía menor cuota de mercado. Para ello adquiere entre 2012 y 2014 los dos grupos financieros nacionalizados
                por el Gobierno de España (Unnim Banc y Catalunya Banc), que agrupaban 6 antiguas cajas de ahorros catalanas.El 7 de marzo de 2012, tras un proceso
                público de adjudicación pública, Unnim Banc fue adjudicado a BBVA por 1 euro, imponiéndose como la mejor opción al no pedir ayudas de capital o liquidez.
                El proceso de adjudicación se llevó a término el 27 de julio de 2012, cuando BBVA completó la compra del 100% de Unnim Banc tras haber obtenido las
                autorizaciones pertinentes de las autoridades comunitarias.Unnim se mantuvo como entidad independiente dentro del grupo BBVA durante un año después de
                su compra. El 15 de marzo de 2013, BBVA aprobó la fusión por absorción de esta entidad con el traspaso en bloque al primero del patrimonio de la sociedad absorbida.
                Entre los días 24 y 26 de mayo de 2013, BBVA culminó la integración de Unnim Banc, por lo que se abandonó la marca Unnim y todas sus oficinas cambiaron su
                rotulación e imagen a la de BBVA.El 21 de julio de 2014, BBVA logró hacerse con Catalunya Banc por 1.187 millones de euros mediante un procedimiento de subasta.
                7​ El 24 de abril de 2015, BBVA completó la compra de Catalunya Banc, una vez formalizada la adquisición del 98,4% del capital social por 1.165 millones de euros.8​
            </p>

            <p>
                La presencia internacional de BBVA comienza en 1902, cuando el Banco de Bilbao abrió una sucursal en París y en 1918 otra en Londres, convirtiéndose así en el primer
                banco español con presencia en el extranjero.En la década de 1970, Banco de Bilbao, Banco de Vizcaya y Banco Exterior se van configurando como grupos internacionales, 
                con la instalación de oficinas operativas y de representación en las capitales financieras de Europa, América y Asia. Así mismo, el banco inició una política de expansión 
                en América mediante la compra de bancos locales en diversos países del continente.
            </p>
            <p>
                El Banco de Vizcaya adquirió en 1979 el Banco Comercial de Mayagüez en Puerto Rico, banco fundado en 1967 por un grupo de comerciantes e industriales. Convertido en BBVA
                Puerto Rico, en 1992 inicia una etapa de crecimiento vía adquisiciones, que dieron origen a BBVA Puerto Rico.En 1995, el grupo entra en Perú, con la privatización
                y posterior adquisición del Banco Continental, y en México, con la compra de Probursa que posteriormente se fusiona con BBVA Bancomer para constituir el grupo 
                financiero BBVA Bancomer que opera en el sector bancario y sector seguros.En 1996, entró en Colombia, con la adquisición de Banco Ganadero y en Argentina, con la adquisición
                de BBVA Francés. Asimismo, realiza nuevas adquisiciones en México, comprando Banca Cremí y Banco de Oriente.En 1997, entró en Venezuela al adquirir el Banco Provincial,
                el cual había sido fundado en 1953. Asimismo, amplía su presencia en Argentina con la adquisición del Banco de Crédito Argentino. También entra en el el negocio de 
                Fondos de Pensiones en Bolivia fundando BBVA Previsión AFP.En 1998, se instala en Chile tras comprar el Banco BHIF, y AFP Provida un año después. Asimismo, entra en 
                Brasil con la compra de Banco Excel-Econômico, y en Argentina compra la aseguradora Consolidar, creada en 1994.En el año 2000 se produce en México la fusión de BBV Probursa
                con Bancomer para crear BBVA Bancomer, el primer banco del país por volumen de activos. A comienzo del año 2004, el Grupo anunció la OPA sobre el 100% de las acciones de 
                Bancomer que aún no pertenecían a BBVA, comprando la totalidad de las acciones del banco mexicano. En 2004 adquiere el 100% de Hipotecaria Nacional, una entidad privada 
                especializada en el negocio hipotecario.En 2001, finaliza la implementación de la plataforma unificada para todos los negocios y todos los países, y se instala la marca
                BBVA en las entidades del Grupo en América Latina.En 2004, los bancos de Chile (BHIF) y Colombia (Banco Ganadero) cambian su denominación comercial y pasan a llamarse
                simplemente BBVA.En 2006, mediante subasta pública adquiere la antigua corporación de ahorro y vivienda, Banco Granahorrar de Colombia, cuyas acciones pertenecían al 
                grupo grancolombiano, y con la crisis financiera y económica de finales de los años 90s pasó a manos del estado a través de Fogafin. Posteriormente lo fusiona con
                BBVA Colombia, creando uno de los grupos bancarios más grande del país.En 2017 BBVA adquiere la empresa mexicana Openpay, startup fintech especializada en pagos online.
            </p>
        </div>
    );
};

export default DemoContent;
