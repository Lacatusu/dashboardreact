import React from 'react';
import {Redirect} from 'react-router-dom';
import {FuseUtils} from '@fuse/index';
import { LoginConfig } from 'main/content/login/LoginConfig';
import {DashboardConfig} from 'main/content/dashboard/DashboardConfig';
import {PersonalConfig} from 'main/content/PersonalOrg/PersonalConfig';
import {Error404PageConfig} from 'main/content/404/Error404PageConfig';

const routeConfigs = [
    ...PersonalConfig,
    LoginConfig,
    DashboardConfig,
    Error404PageConfig
];

export const routes = [
    ...FuseUtils.generateRoutesFromConfigs(routeConfigs),
    {
        path     : '/',
        component: () => <Redirect to="/login"/>
    }
];
