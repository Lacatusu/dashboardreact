export const fuseNavigationConfig = [
    {
        'id'      : 'dashboard-interface',
        'title'   : 'Dashboard',
        'type'    : 'group',
        'icon'    : 'web',
        'children': [
            {
                'id'   : 'resumen-interface',
                'title': 'Dashboard Personal',
                'type' : 'item',
                'icon' : 'dashboard',
                'url'  : '/dashboard'
            }
        ]
    },
    {
        'id'       : 'personalOrg-interface',
        'title'    : 'Organización',
        'type'     : 'group',
        'icon'     : 'web',
        'children' : [
            {
                'id'    : 'toDo-interface',
                'title' : 'To-Do',
                'type'  : 'item',
                'icon'  : 'check_box',
                'url'   : '/organization/to-do'
            },
            {
                'id'    : 'sales-interface',
                'title' : 'Universidad',
                'type'  : 'item',
                'icon'  : 'check_box ',
                'url'   : '/finance/sales'
            },
            {
                'id'    : 'reports-interface',
                'title' : 'Proyectos Personales',
                'type'  : 'item',
                'icon'  : 'web',
                'url'   : '/finance/reports'
            }
        ]
    },
    {
        'id'       : 'netflix-interface',
        'title'    : 'Netflix',
        'type'     : 'group',
        'icon'     : 'important_devices',
        'children' : [
            {
                'id'    : 'movies-interface',
                'title' : 'Peliculas',
                'type'  : 'item',
                'icon'  : 'local_movies',
                'url'   : '/netflix/movies'
            },
            {
                'id'    : 'series-interface',
                'title' : 'Series',
                'type'  : 'item',
                'icon'  : 'live_tv ',
                'url'   : '/netflix/series'
            }
        ]
    }
];
