import axios from 'axios/index';
import firebaseService from 'firebaseService';
import {setUserData} from 'auth/store/actions/user.actions';
import * as Actions from 'store/actions';
import jwt_decode from 'jwt-decode';
import setAuthToken from './setAuthToken';

export const LOGIN_ERROR = 'LOGIN_ERROR';
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';

export const submitLogin = (user) => dispatch => {
    axios.post('http://172.18.0.1:9095/login', user)
            .then(res => {
                const { token } = res.data;
                localStorage.setItem('jwtToken', token);
                setAuthToken(token);
                const decoded = jwt_decode(token);
                dispatch(setCurrentUser(decoded));
                
            })
            .catch(err => {
                dispatch({
                    type: LOGIN_ERROR,
                    payload: err.response.data
                });
            });
}

export const setCurrentUser = decoded => {
    return {
        type: LOGIN_SUCCESS,
        payload: decoded
    }
}
/*
export function loginWithFireBase({username, password})
{
    return (dispatch) =>
        firebaseService.auth && firebaseService.auth.signInWithEmailAndPassword(username, password)
            .then(() => {
                return dispatch({
                    type: LOGIN_SUCCESS
                });
            })
            .catch(error => {
                const usernameErrorCodes = [
                    'auth/email-already-in-use',
                    'auth/invalid-email',
                    'auth/operation-not-allowed',
                    'auth/user-not-found',
                    'auth/user-disabled'
                ];
                const passwordErrorCodes = [
                    'auth/weak-password',
                    'auth/wrong-password'
                ];

                const response = {
                    username: usernameErrorCodes.includes(error.code) ? error.message : null,
                    password: passwordErrorCodes.includes(error.code) ? error.message : null
                };

                if ( error.code === 'auth/invalid-api-key' )
                {
                    dispatch(Actions.showMessage({message: error.message}));
                }

                return dispatch({
                    type   : LOGIN_ERROR,
                    payload: response
                });
            });
}
*/