import * as Actions from '../actions';
import isEmpty from './is-empty';

const initialState = {
    isAuthenticated: false,
    user: {}
};

const auth = function (state = initialState, action) {
    
    switch ( action.type )
    {
        case Actions.LOGIN_SUCCESS:
        {   
            return {
                ...initialState,
                isAuthenticated: !isEmpty(action.payload),
                user: action.payload
            };
        }
        case Actions.LOGIN_ERROR:
        {   console.log(state)
            return {
                isAuthenticated: false,
                error  : action.payload
            };
        }
        default:
        {
            return state
        }
    }
};

export default auth;