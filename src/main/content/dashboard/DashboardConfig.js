import FuseLoadable from '@fuse/components/FuseLoadable/FuseLoadable';
import Dashboard from './Dashboard';


export const DashboardConfig = {
    settings: {
        layout: {
            config: {
                footer        : {
                    display: true
                }
            }
        }
    },
    routes: [{
        path: '/dashboard',
        component: Dashboard
    }]
}