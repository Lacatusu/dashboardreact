import React, {Component} from 'react'
import {Link, withRouter} from 'react-router-dom';
import {withStyles} from '@material-ui/core/styles/index';
import {Card, CardContent, Typography, Icon, Tabs, Tab} from '@material-ui/core';
import classNames from 'classnames';
import {FuseAnimate} from '@fuse';
import RegularLoginTab from './tabs/BasicAuth';

const styles = theme => ({
    root : {
        background    : "url('/assets/images/backgrounds/wallpaper.jpg') no-repeat",
        backgroundSize: 'cover'
    },
    intro: {
        color: '#ffffff'
    },
    card : {
        width   : '100%',
        maxWidth: 400
    }
});

class Login extends Component {
    state = {
        tabValue: 0
    };

    handleTabChange = (event, value) => {
        this.setState({tabValue: value});
    };

    render()
    {
        const {classes} = this.props;
        const {tabValue} = this.state;

        return (
            <div className={classNames(classes.root, "flex flex-col flex-1 flex-no-shrink p-24 md:flex-row md:p-0")}>
                <FuseAnimate animation={{translateX: [0, '100%']}}>
                    <Card className={classNames(classes.card, "mx-auto m-16 md:m-0")}>
                        <CardContent className="flex flex-col items-center justify-center p-32 md:p-48 md:pt-128 ">
                            <Typography variant="h6" className="text-center md:w-full mb-48">Iniciar Sesión</Typography>
                                <Tabs
                                    value={tabValue}
                                    onChange={this.handleTabChange}
                                    fullWidth={true}
                                    className="mb-32"
                                >       
                                <Tab
                                    icon={<Icon className="h-40 text-40">security</Icon>}
                                    className="min-w-0"
                                    label="Autenticar"
                                />
                                </Tabs>

                            {tabValue === 0 && <RegularLoginTab/>}

                    <div className="flex flex-col items-center justify-center pt-32">
                        <span className="font-medium">¿No estás registrado?</span>
                        <Link className="font-medium" to="/register">Crear cuenta</Link>
                    </div>
                        </CardContent>
                    </Card>
                </FuseAnimate>
            </div>
        )
    }
}

export default withStyles(styles, {withTheme: true})(withRouter(Login));