import {ReportsConfig} from './reports/ReportsConfig';
import {SalesAnalysisConfig} from './Sales/SalesConfig';
import {TodoAppConfig} from './todo/TodoAppConfig'
export const PersonalConfig = [
    ReportsConfig,
    SalesAnalysisConfig,
    TodoAppConfig
];
