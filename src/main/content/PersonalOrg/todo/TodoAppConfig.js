import React from 'react';
import {Redirect} from 'react-router-dom';
import {FuseLoadable} from '@fuse';

export const TodoAppConfig = {
    settings: {
        layout: {}
    },
    routes  : [
        {
            path     : '/organization/to-do/label/:labelHandle/:todoId?',
            component: FuseLoadable({
                loader: () => import('./TodoApp')
            })
        },
        {
            path     : '/organization/to-do/filter/:filterHandle/:todoId?',
            component: FuseLoadable({
                loader: () => import('./TodoApp')
            })
        },
        {
            path     : '/organization/to-do/:folderHandle/:todoId?',
            component: FuseLoadable({
                loader: () => import('./TodoApp')
            })
        },
        {
            path     : '/organization/to-do',
            component: FuseLoadable({
                loader: () => import('./TodoApp')
            })
        }
    ]
};
