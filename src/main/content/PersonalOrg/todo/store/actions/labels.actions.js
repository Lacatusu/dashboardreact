import axios from 'axios/index';

export const GET_LABELS = '[TODO APP] GET LABELS';

export function getLabels()
{
    const request = axios.get('http://172.18.0.1:9095/api/todo-app/labels');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_LABELS,
                payload: response.data
            })
        );
}
