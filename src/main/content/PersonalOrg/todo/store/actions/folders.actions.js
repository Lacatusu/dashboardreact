import axios from 'axios/index';

export const GET_FOLDERS = '[TODO APP] GET FOLDERS';

export function getFolders()
{
    const request = axios.get('http://172.18.0.1:9095/api/todo-app/folders');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_FOLDERS,
                payload: response.data
            })
        );
}