import React from 'react';
import {FuseLoadable} from '@fuse';

export const SalesAnalysisConfig = {
    settings: {
        layout: {}
    },
    routes  : [
        {
            path     : '/finance/sales',
            component: FuseLoadable({
                loader: () => import('./Sales')
            })
        }
    ]
};
