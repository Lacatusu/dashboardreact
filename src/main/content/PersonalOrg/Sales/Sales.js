import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import {FusePageSimple, DemoContent} from '@fuse';
import * as Actions from 'auth/store/actions';
import {withRouter} from 'react-router-dom';
import connect from 'react-redux/es/connect/connect';
import PropTypes from 'prop-types';

const styles = theme => ({
    layoutRoot: {}
});

class Example extends Component {
    componentDidMount() {
        if (!this.props.login.isAuthenticated )
        {
            this.props.history.push('/not-found');
        }
      }
    render()
    {
        const {classes} = this.props;
        return (
            <FusePageSimple
                classes={{
                    root: classes.layoutRoot
                }}
                header={
                    <div className="p-36" style={{marginLeft:"40%"}}><h4>Este es el controlador de Ventas</h4></div>
                }
                content={
                    <div className="p-24">
                        <br/>
                    </div>
                }
            />
        )
    }
}

function mapStateToProps({auth})
{   console.log(auth.login);
    return {
        login: auth.login,
        user : auth.user
    }
}


export default withStyles(styles, {withTheme: true})(withRouter(connect(mapStateToProps)(Example)));