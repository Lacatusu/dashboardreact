import React from 'react';
import {FuseLoadable} from '@fuse';

export const ReportsConfig = {
    settings: {
        layout: {}
    },
    routes  : [
        {
            path     : '/finance/reports',
            component: FuseLoadable({
                loader: () => import('./Reports')
            })
        }
    ]
};
